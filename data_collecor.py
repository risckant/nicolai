#!/usr/bin/env python
#used for data collection, in order to train the neural network.
import driver
import cv2
from imutils.video import VideoStream
from imutils.video import FPS
import os
import sys
import random
from time import sleep
import darknet
import paramiko

if len(sys.argv) < 4:
    print("usage: ./data_collecor.py <ip of fpga> <username> <password>")
    exit(1)

vs = VideoStream(src=0).start()
ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname = sys.argv[1], username = sys.argv[2], password = sys.argv[3],look_for_keys=False, allow_agent=False)
path= "/home/ba/IaC/cig_data/images/"
summa = 33
while True:
    a = input("wieviele Bilder")
    for i in range(0, int(a)):
        driver.rotate_angle(random.gauss(0,13))
        sleep(1)
        frame = vs.read()
        cv2.imwrite(str(summa)+".jpg", frame)
        print(str(summa)+".jpg")
        darknet.uploadFile(str(summa)+".jpg", path + str(summa)+".jpg", ssh_client)
        os.remove(str(summa)+".jpg")
        summa= summa +1