#!/usr/bin/env python
#implements the robot flow
from __future__ import division
from imutils.video import VideoStream
from imutils.video import FPS
import sys
import imutils
import threading
import time
import cv2
import json
import subprocess
import random
import driver
import paramiko
import Queue


if len(sys.argv) < 4:
    print("usage: ./data_collecor.py <ip of fpga> <username> <password> <optional option 'fpga'>")
    exit(1)

def rotater(frame, x, y, w, h):
    height, width = frame.shape[:2]        
    x_centre = width/2
    print("Middle of image: "+str(x_centre))
    x_roi = x + w/2
    print("x of cig_butt: " + str(x_roi))
    diff = x_roi-x_centre
    angle = 45/(width/diff)
    angle = angle/1.5
    print(angle)
    if diff > 12:
        print("rotating left because of diff: " + str(diff))
        driver.rotate_angle(-angle) 
        return False 
    if diff < -12: 
        print("rotating right because of diff: " + str(diff))
        driver.rotate_angle(-angle)
        return False
    else:
        return True

def aligner(frame, x, y, w, h):
    height, width = frame.shape[:2]        
    y_roi = y+h/2
    diff = height-y_roi
    if y_roi > 0.8*height:
        print("too far away: " + str(y_roi))
        driver.drive_cm(100)
    else:
        real = y_roi/(0.8*height)
        cm = (200*(0.5*real))/3
        print("y_roi: " + str(y_roi)+"\ndiff: "+ str(diff)+"\nheight: "+str(height)+"\n cm: " + str(cm) + "\ny: "+str(y))
        print(cm)
        if y_roi > 80:
            driver.drive_cm(cm)
            return True
        else:
            print("end")
            driver.clean_start()
            driver.drive_cm(60)
            driver.clean_stop()
            return False

    driver.drive_start(True)
def fpga_calc():
    global ongoing_calc
    global ssh_client
    global path
    global cd
    global classify
    ongoing_calc = True
    #upload current.jpg
    darknet.executeCommand("rm " + path + "coordinates.json", ssh_client)
    darknet.uploadFile("current.jpg", path + "current.jpg", ssh_client)
    #execute command
    darknet.executeCommand(cd + classify, ssh_client)
    #download prediction.jpg
    darknet.downloadFile(path + "predictions.jpg", "predictions.jpg", ssh_client)
    darknet.downloadFile(path + "coordinates.json", "coordinates.json", ssh_client)
    ongoing_calc=False


s_fpga = (len(sys.argv) == 5) and (sys.argv[4] == "fpga")
if s_fpga:    
    import fpga as darknet
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname = sys.argv[1], username = sys.argv[2], password = sys.argv[3],look_for_keys=False, allow_agent=False)

else:
    import darknet
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname = sys.argv[1], username = sys.argv[2], password = sys.argv[3],look_for_keys=False, allow_agent=False)


vs = VideoStream(src=0).start()
#create tracker
tracker = cv2.TrackerKCF_create()


#command for detection
if s_fpga:
    classify = "./darknet detector test data/cig.data cfg/cig_no1.cfg cig_no1.weights current.jpg"
    path = "/home/ba/IaC/darknet-mod/darknet/"
    cd = "cd " + path + ";"
else:
    classify = "./darknet.elf detector test data/cig.data cfg/cig_no1.cfg cig_no1.weights current.jpg"
    path="/home/root/darknet/"
    cd = "cd " + path + ";"
#loop
while True:
    frame = vs.read()
    cv2.imwrite("current.jpg", frame)
    fpga_calc()
    coordinates_f = open("coordinates.json")
    coordinates = json.loads(coordinates_f.read())
    coordinates = coordinates["objects"]
    #if cig-but is detected, select the one with the highest propability
    if coordinates:
        print("found cig_butt\n")
        prediction = cv2.imread("predictions.jpg")
        #cv2.imshow("prediction",prediction)
        #cv2.waitKey(1)
        high_prob = coordinates[0] #var for holding the object with highest prob
        for dicti in coordinates: #find the obj with the highest prob
            if high_prob['prob'] < dicti['prob']:
                high_prob = dicti
        roi = (int(high_prob['left_x']), int(high_prob['top_y']), int(high_prob['width']), int(high_prob['height']))
        print(roi)
        tracker = cv2.TrackerKCF_create()
        frame = vs.read()
        tracker.init(frame, roi)
        frame_error = 0
        ongoing_calc = False
        cv2.imwrite("current.jpg", frame)
        x = threading.Thread(target=fpga_calc)
        x.start()
        while frame_error < 20 and ongoing_calc: 
            frame = vs.read()
            #update the tracker with new frame
            (success, BB) = tracker.update(frame)
            print(success)
            if success:
                #extract the coordinates
                (x, y, w, h) = [int(v) for v in BB]
                print(BB)
                if x > 10:
                    #draw the rectangle
                    cv2.rectangle(frame, (x, y), (x + w, y + h),(0, 255, 0), 2)
                    #show frame
                    cv2.imshow("Frame", frame)
                    cv2.waitKey(1)
                    time.sleep(0.3)
                    print("rotating")
                    if rotater(frame, x, y, w, h): #rotate so that robot face cig_but
                        aligner(frame,x,y,w,h)
                else:
                    print("tracker bug")
            else:
                frame_error = frame_error+1
                cv2.imshow("Frame", frame)
                cv2.waitKey(1)
        print("waiting for fpga")
        while ongoing_calc:
            pass
        print("reading new coordinates")
        coordinates_f = open("coordinates.json")
        coordinates = json.loads(coordinates_f.read())
        coordinates = coordinates["objects"]


            
    else:
        print("didn't detect cig_butt\nturning around")
        driver.rotate_angle(random.gauss(0,30))
        time.sleep(1)