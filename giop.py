import RPi.GPIO as GPIO
from time import sleep
GPIO.setmode(GPIO.BCM)
GPIO.setup(19, GPIO.OUT)
GPIO.output(19, GPIO.HIGH)
sleep(3)
GPIO.output(19, GPIO.LOW)
sleep(3)
GPIO.output(19, GPIO.HIGH)
sleep(3)
GPIO.cleanup()
