#driving module
from __future__ import division
import sys
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

right1, right2 = 16, 20
left1, left2 = 19, 26
vac = 13
servo = 17

GPIO.setup(left1, GPIO.OUT)#left motor
GPIO.setup(left2, GPIO.OUT)#left motor
GPIO.setup(right1, GPIO.OUT)#right motor
GPIO.setup(right2, GPIO.OUT)#right motor
GPIO.setup(vac, GPIO.OUT)#cleaner
GPIO.setup(vac, GPIO.OUT)
GPIO.output(vac, True)#cause of relay
def drive(s, direction):
    #direction is +y if True and -y if False
    print("in drive")
    if direction:
        GPIO.output(left1, True)
        GPIO.output(right1, True)
        time.sleep(s)
        GPIO.output(left1, False)
        GPIO.output(right1, False)
    else:
        GPIO.output(left2, True)
        GPIO.output(right2, True)
        time.sleep(s)
        GPIO.output(left2, False)
        GPIO.output(right2, False)
        
def drive_start(direction):
    if direction:
        GPIO.output(left1, True)
        GPIO.output(right1, True)
    else:
        GPIO.output(left2, True)
        GPIO.output(right2, True)

def drive_stop(direction):
    if direction:
        GPIO.output(left1, False)
        GPIO.output(right1, False)
    else:
        GPIO.output(left2, False)
        GPIO.output(right2, False)

def drive_slow(s, direction):
    for i in range(0,int((s/0.075))):
        if direction:
            time.sleep(0.05)
            GPIO.output(left1, True)
            GPIO.output(right1, True)
            time.sleep(0.025)
            GPIO.output(left1, False)
            GPIO.output(right1, False)
        else:
            GPIO.output(left2, True)
            GPIO.output(right2, True)
            time.sleep(0.1)
            GPIO.output(left2, False)
            GPIO.output(right2, False)

def drive_cm(cm):
    #24 cm in 1s
    sec = 1/24*abs(cm)
    if cm < 0:
        drive(sec, False)
    else: 
        drive(sec, True)

def rotate(s, direction):
    if direction:#links rum
        GPIO.output(left1, True)
        #GPIO.output(right2, True)
        time.sleep(s)
        GPIO.output(left1, False)
        #GPIO.output(right2, False)

    else:#rechts rum
        GPIO.output(left2, True)
        #GPIO.output(right1, True)
        time.sleep(s)
        GPIO.output(left2, False)
        #GPIO.output(right1, False)


def rotate_start(direction):
    if direction:#links rum
        GPIO.output(left1, True)
        #GPIO.output(right2, True)
    else:#rechts rum
        #GPIO.output(left2, True)
        GPIO.output(right1, True)
    
def rotate_stop(direction):
    if direction:
        GPIO.output(left1, False)
        #GPIO.output(right2, False)
    else:
        #GPIO.output(left2, False)
        GPIO.output(right1, False)

def rotate_angle(angle):
    #65 grad in 1s
    sec = 1/65 * abs(angle)
    if angle < 0:
        rotate(sec,True)
    else:
        rotate(sec, False)

def clean(s):
    GPIO.output(vac, False)
    sleep(s)
    GPIO.output(vac, True)

def clean_start():
    GPIO.output(vac, False)

def clean_stop():
    GPIO.output(vac, True)

