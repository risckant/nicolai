import paramiko

def executeCommand(command,client):
    stdin,stout,stderr = client.exec_command(command)
    exit_status = stout.channel.recv_exit_status()  
    return stdin, stout, stderr

def uploadFile(local_path, remote_path, client):
    ftp_client = client.open_sftp()
    ftp_client.put(local_path, remote_path)
    ftp_client.close()

def downloadFile(remote_path, local_path, client):
    try:
        ftp_client = client.open_sftp()
        ftp_client.get(remote_path, local_path)
        ftp_client.close()
    except:
        print("file not found")

