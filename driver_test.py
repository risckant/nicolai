#!/usr/bin/env python
import driver
import time
import threading
import Queue
import pygame
import sys

yes = raw_input("live(1) or programmed(2)")
if yes == "1":
    print("use wasd or arrow keys for movement")
    pygame.init()
    screen = pygame.display.set_mode((900,500))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT or event.key == ord('a'):
                    print('left')
                    driver.rotate_start(True)
                if event.key == pygame.K_RIGHT or event.key == ord('d'):
                    print('right')
                    driver.rotate_start(False)
                if event.key == pygame.K_UP or event.key == ord('w'):
                    print('forward')
                    driver.drive_start(True)
                if event.key == pygame.K_DOWN or event.key == ord('s'):
                    print('backwards')
                    driver.drive_start(False)
                if event.key == pygame.K_SPACE:
                    print('clean')
                    driver.clean_start()

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == ord('a'):
                    print('left stop')
                    driver.rotate_stop(True)
                if event.key == pygame.K_RIGHT or event.key == ord('d'):
                    print('right stop')
                    driver.rotate_stop(False)
                if event.key == pygame.K_UP or event.key == ord('w'):
                    print('forward stop')
                    driver.drive_stop(True)
                if event.key == pygame.K_DOWN or event.key == ord('s'):
                    print('backwards stop')
                    driver.drive_stop(False)
                if event.key == pygame.K_SPACE:
                    print('clean stop')
                    driver.clean_stop()
                if event.key == ord('q'):
                    pygame.quit()
                    sys.exit()
elif yes == "2":
    while True:
        direct = raw_input("which direction, use arrow keys! ")
        timer = raw_input("how many seconds? ")
        print("taken input: " + direct)
        if direct == 'w':
            print("forward")
            driver.drive(int(timer),True)
        elif direct == "s":
            print("backward")
            driver.drive(int(timer),False)
        elif direct == "a":
            print("left")
            driver.rotate(int(timer),True)
        elif direct == "d":
            print("right")
            driver.rotate(int(timer), False)
        else:
            print("unknown key")
else:
    print("unknwon command")
