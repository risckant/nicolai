# nicolAI - practical demonstration of RISCkant
## Main information

* the main program flow can be found in tracker.py and is implemented as shown in the flowchart.
![alt text](https://i.ibb.co/9skd8jX/flower.png "Flowchart of robot")
 
* driver.py is the driver module for controlling motor driver and vaccum cleaner
* darknet.py or fpga.py are for the communication to the FPGA
* driver_test.py is a test for the driving part
* tracker_test.py is a test for the basic object-tracking function
* data-collecor.py is used for image collection, in order to train a neural network, used by RISCkant
* demo.py shows the functionality of the FPGA
