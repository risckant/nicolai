#!/usr/bin/env python
import paramiko

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname = "fe80::20a:35ff:fe00:1e53%eth0", username = "root", password = "root")
ftp_client = ssh_client.open_sftp()
ftp_client.put("/home/pi/vilenda/fpga.tar","/home/root/fpga.tar") #upload darknet
ftp_client.close()
ssh_client.exec_command("cd /home/root; tar x -f /home/root/fpga.tar")
ssh_client.exec_command("rm /home/root/fpga.tar")
ssh_client.exec_command("/sbin/modprobe xilinx-axidma")
print("execution finished")
ssh_client.close()
