#!/usr/bin/env python
from __future__ import division
from imutils.video import VideoStream
from imutils.video import FPS
import sys
import imutils
import threading
import time
import cv2
import json
import subprocess
import random
import driver
import paramiko
import Queue

def rotater(frame, x, y, w, h):
    height, width = frame.shape[:2]        
    x_centre = width/2
    print("Middle of image: "+str(x_centre))
    x_roi = x + w/2
    print("x of cig_butt: " + str(x_roi))
    diff = x_roi-x_centre
    angle = 45/(width/diff)
    angle=
    if diff < 10:
        print("rotating left because of diff: " + str(diff))
        driver.rotate_angle()
        return False 
    if diff > 10: 
        print("rotating right because of diff: " + str(diff))
        driver.rotate_start(False)   
        return False
    else:
        driver.rotate_stop()
        return True

def aligner(frame, x, y, w, h):
    height, width = frame.shape[:2]        
    y_roi = y + h/2
    diff = height=y_roi
    if diff > 30:
        driver.drive_start(True)
        return True
    else:
        driver.drive_stop()
        return False

    driver.drive_start(True)
def fpga_calc():
    ongoing_calc = True
    #upload current.jpg
    darknet.uploadFile("current.jpg", path + "current.jpg", ssh_client)
    #execute command
    darknet.executeCommand(cd + classify, ssh_client)
    #download prediction.jpg
    darknet.downloadFile(path + "predictions.jpg", "predictions.jpg", ssh_client)
    darknet.downloadFile(path + "coordinates.json", "coordinates.json", ssh_client)


s_fpga = (len(sys.argv) == 2) and (sys.argv[1] == "fpga")
if s_fpga:    
    import fpga as darknet
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname = "192.168.178.71", username = "ba", password = "2309",look_for_keys=False, allow_agent=False)

else:
    import darknet
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname = "fe80::20a:35ff:fe00:1e53%eth0", username = "root", password = "root",look_for_keys=False, allow_agent=False)


vs = VideoStream(src=0).start()
#command for detection
if s_fpga:
    classify = "./darknet detector test data/cig.data cfg/cig_no1.cfg cig_no1.weights current.jpg"
    path = "/home/ba/IaC/darknet-mod/darknet/"
    cd = "cd " + path + ";"
else:
    classify = "./darknet.elf detector test data/cig.data cfg/cig_no1.cfg cig_no1.weights current.jpg"
    path="/home/root/darknet/"
    cd = "cd " + path + ";"
#loop
tracker = cv2.TrackerKCF_create()
while True:
    frame = vs.read()
    cv2.imwrite("current.jpg", frame)
    fpga_calc()
    coordinates_f = open("coordinates.json")
    coordinates = json.loads(coordinates_f.read())
    coordinates = coordinates["objects"]
    #if cig-but is detected, select the one with the highest propability
    if coordinates:
        print("found cig_butt\n")
        prediction = cv2.imread("predictions.jpg")
        cv2.imshow("prediction",prediction)
        cv2.waitKey(0)
        high_prob = coordinates[0] #var for holding the object with highest prob
        for dicti in coordinates: #find the obj with the highest prob
            if high_prob['prob'] < dicti['prob']:
                high_prob = dicti
        print("highest propability:\n"+str(high_prob))
        roi = (int(high_prob['left_x']), int(high_prob['top_y']), int(high_prob['width']), int(high_prob['height']))
        #roi = frame[int(high_prob['left_x']): int(high_prob['top_y']), int(high_prob['width']): int(high_prob['height'])]
        tracker.init(frame, roi)
        
        while True: 
            time.sleep(0.1)
            frame = vs.read()
            #update the tracker with new frame
            (success, BB) = tracker.update(frame)
            print(success)
            #extract the coordinates
            (x, y, w, h) = [int(v) for v in BB]
            #(x, y, w, h) = [int(v) for v in roi]
            #draw the rectangle
            cv2.rectangle(frame, (x, y), (x + w, y + h),(0, 255, 0), 2)
            #show frame
            cv2.imshow("Frame", frame)
            key = cv2.waitKey(1) & 0xFF
            
            
    else:
        print("didn't detect cig_butt\nturning around")
        driver.rotate_angle(random.gauss(0,30))
        time.sleep(1)

    










