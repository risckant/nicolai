#!/usr/bin/env python
import cv2
import os
import time
import sys
from pwn import *
import threading

if len(sys.argv) < 2:
    print("usage: ./demo.py <ip of fpga> (image)")
    exit(1)

conn = ssh(host=sys.argv[1], user="root", password="root")
conn.set_working_directory("/home/root/darknet")

IP = sys.argv[1]
cv2.namedWindow("preview")


def detect_hw(path):
    conn.upload_file(path, path)
    conn.run_to_end("./darknet.elf detect cfg/yolov3-tiny.cfg yolov3-tiny.weights {}".format(path))
    sleep(1)
    conn.download("predictions.jpg", "predictions.jpg")

rval = True
while rval:
    vc = cv2.VideoCapture(0)
    rval, frame = vc.read()
    vc.release()
    cv2.imwrite("frame.jpg", frame)
    detect_hw("frame.jpg")
    img = cv2.imread("predictions.jpg")
    cv2.imshow("preview", img)
    key = cv2.waitKey(500)
    if key == 27:
        break

os.remove("predictions.jpg")
os.remove("frame.jpg")
cv2.destroyWindow("preview")